const Transform = require('stream').Transform
const csv = require('csv-streamify')
const wappalyzer = require('wappalyzer')
const csvToJson = csv({ objectMode: true })
const fs = require('fs')
const dataFile = fs.createReadStream('data.csv');

const parser = new Transform({ objectMode: true })

let urlIndex = null

parser._transform = async function(data, encoding, done) {
  const urlColumn = data.indexOf('external_urls/homepage')
  let dataString = data.map(i => `\"${i}\"`).join(',').replace(/\r/, ' ')

  if (~urlColumn) {
    urlIndex = urlColumn
    this.push(`${dataString},With React\n`)
    return done()
  }
  if (~urlIndex) {
  const url = ~data[urlIndex].indexOf('http') ? data[urlIndex] : `http://${data[urlIndex]}`
  if(data[urlIndex]) {
    console.log(url)
    await wappalyzer.analyze(url)
      .then(json => {
        const reactIndex = ~json.findIndex(e => e.name === 'React')
        this.push(`${dataString},${!!reactIndex}\n`)
      })
      .catch(() => {
        this.push(`${dataString},pending\n`)
      })
    } else {
      this.push(`${dataString},URL is Null\n`)
    }
  }
  console.log('done')
  done()
}

dataFile
  .pipe(csvToJson)
  .pipe(parser)
  .pipe(fs.createWriteStream('data.csv', { flags: 'r+' }))

process.stdout.on('error', process.exit)
